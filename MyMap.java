package MyMap;
// тут нет КЧ-деревьев потому что я не додумался как их сделать

// так добавление нового элемента в список быстрее


import java.util.ArrayList;
import java.util.HashSet;

// для себя:
public class MyMap<K, T>{


    public int size(){
        return this.size;
    }
    private int size = 0;


    private LinkedList<K, T> [] Buckets = new LinkedList[16];


    private final double percentOfFulling = 0.7;

    private int bucketsSize= 0;

    public String values(){
        String firstSkobka = "[ ";
        for(LinkedList<K, T> i:this.Buckets){
            if (i != null){
                firstSkobka += i.getValues();
            }
        }
        String lastSkobka = " ]";
        return firstSkobka + lastSkobka;
    }


    public void put(K key, T data){
        ;
        Node<K, T> newNode = new Node<>(key, data);
        int index = this.indexFor(newNode.hash(key), this.Buckets.length);
        if (this.Buckets[index] == null){
            this.bucketsSize++;
            this.size++;
            LinkedList <K, T> link = new LinkedList<K, T>(newNode);
            this.Buckets[index] = link;
        }else{
            if (this.Buckets[index].getByKey(key) == null){
                this.Buckets[index].addNode(newNode);
                this.size++;
            }else{
                this.Buckets[index].getByKey(key).setData(newNode.getData());

            }

        }
        this.updateArray();
    }


    private void updateArray(){
        if (((double) bucketsSize / this.Buckets.length) > 0.7){
            LinkedList<K, T> [] newBuckets = new LinkedList [this.Buckets.length * 2];
            for(int i = 0; i < this.Buckets.length; i++){
                newBuckets[i] = this.Buckets[i];
            }
            this.Buckets = newBuckets;
        }
    }

    public Boolean containsKey(K key){
        for (LinkedList<K, T> i:this.Buckets){
            if (i != null){
                if (i.getByKey(key) != null){
                    return true;
                }

            }
        }
        return false;
    }

    public Boolean containsData(T data){
        for (LinkedList<K, T> i:this.Buckets){

            if (i != null && i.getByData(data) != null){
                return true;
            }
        }
        return false;
    }

    public T get(K key){
        Node<K, T> newNode = new Node<K, T>(key);
        int index;
        index = this.indexFor(newNode.getHash(), this.Buckets.length);
        return this.Buckets[index].getByKey(key).getData();
    }


    private int indexFor(int h, int length) {
        return h & (length-1);
    }

    public void clear(){
        for(int i = 0; i < Buckets.length; i++){
            Buckets[i] = null;
        }
    }

    public Boolean isEmpty(){
        for(LinkedList<K, T> i:this.Buckets){
            if(i != null){
                return false;
            }
        }
        return true;
    }

    public void remove(K key){
        for (int i = 0; i < Buckets.length; i++){
            if (Buckets[i] != null)
                if (Buckets[i].remove(key)){
                    size--;
                    if (Buckets[i].getHead() == null){
                        Buckets[i] = null;
                    }
                }
        }

    }

    public ArrayList<K> keySet(){
        ArrayList<K> set = new ArrayList<>();
        for(LinkedList<K, T> i:Buckets){
            if (i != null){
                set.addAll(i.getKeys());
            }
        }
        return set;
    }




}



class LinkedList<K, T>{


    private Node<K, T> head;

    public Node<K, T> getHead() {
        return head;
    }

    LinkedList(Node<K, T> head) {
        this.head = head;
    }



    public void addNode(Node<K, T> newNode){

        Node<K, T> stepinNode = this.head;
        while (stepinNode.getNext() != null){

            stepinNode = stepinNode.getNext();
        }
        stepinNode.setNext(newNode);

    }


    public String getValues(){
        String lineOfValues = "";
        Node<K, T> stepinNode = this.head;
        while (stepinNode != null){
            lineOfValues += stepinNode.getData() + " ";
            stepinNode = stepinNode.getNext();
        }
        return lineOfValues;
    }

    public ArrayList<K> getKeys(){
        ArrayList<K> keys = new ArrayList<>();

        Node<K, T> stepinNode = this.head;
        while (stepinNode != null){
            keys.add(stepinNode.getKey());
            stepinNode = stepinNode.getNext();
        }
        return keys;
    }

    public Boolean remove(K key){
        Node<K, T> stepinNode = this.head;
        Node<K, T> prevStepinNode;
        while (stepinNode.getNext() != null){
            prevStepinNode = stepinNode;

            if (stepinNode.getKey().equals(key)){
                prevStepinNode.setNext(stepinNode.getNext());
                return true;

            }
            stepinNode = stepinNode.getNext();
        }
        if (stepinNode.getNext() == null & stepinNode.getKey().equals(key)){
            this.head = null;
            return true;
        }
        return false;

    }



    public Node<K, T> getByKey(K key){
        Node<K, T> stepinNode = this.head;
        if (stepinNode.getNext() == null & stepinNode.getKey().equals(key)){
            return stepinNode;
        }



        while (stepinNode.getNext() != null){
            if (stepinNode.getKey().equals(key)){
                return stepinNode;
            }
            stepinNode = stepinNode.getNext();
        }
        return null;
    }




    public Node<K, T> getByData(T data){
        Node<K, T> stepinNode = this.head;
        if (stepinNode.getNext() == null & stepinNode.getData().equals(data)){
            return stepinNode;
        }

        while (stepinNode.getNext() != null) {
            if (stepinNode.getData().equals(data)) {
                return stepinNode;
            }
            stepinNode = stepinNode.getNext();
        }
        return null;
    }
}



class Node <K, T> {


    private T data;


    public void setData(T data) {
        this.data = data;
    }

    private final Integer hash;
    private K key;

    private Node<K, T> next = null;

    public T getData() {
        return this.data;
    }

    public K getKey() {
        return this.key;
    }

    public Node<K, T> getNext() {

        if (this.next == null){
            return null;
        }
        return this.next;
    }


    public void setNext(Node<K, T> nextNode) {
        this.next = nextNode;
    }


    Node(K key) {
        this.hash = this.hash(key);
    }

    Node(K key, T data) {
        this.data = data;
        this.key = key;
        this.hash = this.hash(key);
    }

    public Integer getHash() {
        return hash;
    }

    public Integer hash(K key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }


}

