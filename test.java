package MyMap;

public class test {
    public static void main(String[] args) {
        MyMap<Integer, String> test = new MyMap<Integer, String>();
        test.put(21, "яблоки");
        test.put(32, "уборка");
        test.put(32, "мышцы");
        test.put(1, "ржавый");
        test.put(2, "поезд");
        test.put(4, "буква");
        test.put(9, "взгляд");
        test.put(10, "омлет");
        test.put(11, "яйца");
        test.put(12, "первый");
        test.put(13, "последний");


        test.remove(32);

        System.out.println(test.get(21));
        System.out.println(test.values());
        System.out.println(test.containsKey(32));
        System.out.println(test.containsData("первый"));
        System.out.println(test.isEmpty());
        System.out.println(test.size());
        System.out.println(test.keySet());
        test.clear();
        System.out.println(test.values());


    }
}
